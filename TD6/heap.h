/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heap.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/18 03:50:31 by NoobZik           #+#    #+#             */
/*   Updated: 2018/11/18 04:54:50 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

typedef struct {
  int max; // MAX Malloc
  int n; // Nombre actuelle
  int* tab;
}heap;

heap *init_heap(int m);
void print_heap(heap *h);
int heap_maximum(heap *h);
void heap_insert (int k, heap *h);
void heap_free(heap *h);
int heap_delete(int k, heap *h);
