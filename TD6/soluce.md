# TD 6 - TAS #

### Exercice 2 ###

#### Question 2 ####
1.  Quels sont les nombres minimal et maximal de nœuds d’un tas de hauteur h ?

2.  Quelle est la hauteur d’un tas à n nœuds ?

3.  Montrer que pour un sous-arbre quelconque d’un tas, la racine du sous-arbre
 contient la plus grande valeur parmi les nœuds de ce sous-arbre

4.  Dans un tas où tous les éléments sont distincts, quelles sont les
positions possibles pour le plus petit élément ?
