/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 16:10:06 by NoobZik           #+#    #+#             */
/*   Updated: 2018/11/18 04:57:40 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <stdbool.h>
#include "heap.h"
/**
 * Initialize a heap
 * @param  m [description]
 * @return   [description]
 */
heap *init_heap(int m) {
  heap *res = 0;
  if(!(res = malloc(sizeof(heap))))
    return NULL;
  res->n = 0;
  res->max = m;
  if (!(res->tab = (int *) malloc(sizeof(int) * (size_t)m))) {
    free(res);
    return NULL;
  }
  return res;
}

/**
 * [heap_free description]
 * @param  h [description]
 * @return   [description]
 */
 void heap_free(heap *h) {
  free(h->tab);
  free(h);
}

/**
 * Print the heap array
 * @param h [description]
 */
void print_heap(heap *h) {
  int i = -1;
  while(++i < h->n) {
    printf("%d ", h->tab[i]);
  }
  puts("");
  printf("Printed %d number (%d int array)\n",i,h->n);
}

/**
 * Return the maximum of the heap
 * @param  h [description]
 * @return   [description]
 */
int heap_maximum(heap *h) {
  int i = -1;
  int res = 0;

  while (++i < h->n) {
    if (h->tab[i] > res)
     (res = h->tab[i]);
  }
  return res;
}
/**
 * Heap insertion with a Sift up maximisation
 * @param k [description]
 * @param h [description]
 */
void heap_insert (int k, heap *h) {
  // Heap is empty base case
  if (!(h->n)) {
    h->tab[0] = k;
    h->n += 1;
    return;
  }

  // Heap is not empty, adding at the end of array then Sift up if needed

  int i = h->n;
  int n = i;
  int tmp;

  assert(h->max > h->n+1);
  h->tab[h->n] = k;
  h->n += 1;

  // Sift up if needed

  while ((h->tab[(int) floor((i-1)/2)] < h->tab[n])) {
    tmp = h->tab[(int) floor((i-1)/2)];
    h->tab[(int) floor((i-1)/2)] = h->tab[n];
    h->tab[n] = tmp;
    n = (int) floor((i-1)/2);
    i = n;
  }
}
/**
 * delete the k inside the heap
 * @param  k [description]
 * @param  h [description]
 * @return   [description]
 */
int heap_delete(int k, heap *h) {
  (void) h;
  return k;
}
