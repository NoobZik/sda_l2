/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/18 03:50:59 by NoobZik           #+#    #+#             */
/*   Updated: 2018/11/18 04:40:59 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include "heap.h"

int main(int argc, char const *argv[]) {
  (void) argc;
  (void) argv;

  heap *h = init_heap(20);
  heap_insert(55,h);
  heap_insert(24,h);
  heap_insert(18,h);
  heap_insert(14,h);
  heap_insert(11,h);
  heap_insert(16,h);
  heap_insert(9,h);
  heap_insert(3,h);
  heap_insert(12,h);
  heap_insert(6,h);
  heap_insert(1,h);
  heap_insert(7,h);
  heap_insert(43,h);
  print_heap(h);
  heap_free(h);
  return 0;
}
