/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TP1.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/30 09:03:02 by NoobZik           #+#    #+#             */
/*   Updated: 2018/09/21 15:40:53 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

#define TRUE  1
#define FALSE 0

int   isSameArray       (int *, int *, int, int);
int   averageArray      (int *, int);
void  reverseArray      (int *, int);
int   isSortedStricly   (int *, int);

int main(int argc, char const *argv[]) {
  int tab[5] = {1,2,10,4,5};
  int i = -1;

  (void) argc;
  (void) argv;

  char const *tab1[2] = {"not sorted","strictly sorted"};
  //tab = malloc(sizeof(int)*5);
/*
  while(++i < 5){
    tab[i] = i;
  }*/

  printf("The array is currently %s\n", tab1[isSortedStricly(tab,5)]);
  printf("The average is %d\n", averageArray(tab,5));
  reverseArray(tab,5);
  printf("The reversed array is :\n");
  while(++i<5){
    printf("%d ", tab[i]);
  }
  printf("\n");

  return 0;
}

/**
 * Return a boolean statement for comparing 2 Arrays
 * @param tabA First array
 * @param tabB second array
 * @param k    lenght of the first array
 * @param l    lenght of the second array
 * @return A boolean statement
 */
int isSameArray(int *tabA, int *tabB, int k, int l){
  int i = -1;

  if(!(k == l)) return FALSE;
  while(++i < k){
    if(!(tabA[i] == tabB[i]))
      return FALSE;
  }
  return TRUE;
}

/**
 * [averageArray description]
 * @param tab [description]
 * @param n   [description]
 * @return [description]
 */
int averageArray(int *tab, int n){
  int sum = 0,
        i = -1;
  while(++i < n){
    sum += tab[i];
  }
  return sum /= n;
}

void reverseArray(int *tab, int n){
  int i = -1;
  int tmp;

  while(++i < --n){
    tmp = tab[i];
    tab[i] = tab[n];
    tab[n] = tmp;
  }

  return;
}

int isSortedStricly(int *tab, int n){
  int i = -1;

  while(++i < n--){
    printf("%d | %d -- %d | %d\n", tab[i], tab[n], i, n);
    if(tab[i] <= tab[n]){
      continue;
    }
    else return FALSE;
  }
  return TRUE;
}
