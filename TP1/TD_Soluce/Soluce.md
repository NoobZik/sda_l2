# TD1 : Revision du langage C #

---

## Note ##

Pratique du copié / collé du tableau, avec en complément des photos et des recherches tableau.

---

### Exerce 1 : Question de compréhension ###

1)  C'est une variable qui contient une adresse
2)  On appelle indrection l'accès indirect à un objet via un  pointeur

Exemple :

```c
int a, b;
a = 5;
b = *(&a); /* *(Trois egale) a=n */
```

3) Quelle est l'erreur

```c
  int ptr;
  ptr = 5;
  cout << ptr;
  return 0;
```
Il y a pas d'allocation de mémoire

4)  Test
  \*p1++ Incrémente le tableau.
  (\*p1)++ Incrémente la valeur.
  \*(p1++) Incrémente l'adresse.

5) &ptr = Adresse du pointeur
6) \*(&a) = Pointeur sur l'adresse de a

### Exercice 2 : ###


```c
float varFloat = 1.7;
float *ptr = &varFloat;

printf("%x\n",&varFloat);
printf("%f\n", *ptr);
```

### Exercice 3: ###

```c
int a = 10;
int b = 20;

int *iptrA = &a;
int *iptrB = &b;

*iptrA = *iptrA + *iptrB;

printf("&iptrA %x &iptrB %x\n", iptrA, iptrB);
printf("*iptrA %d *iptrB %d\n", *iptrA, *iptrB);

```


### Exercice 4 ###

Rechercher l'erreur
C'est un pointeur static, on ne peut pas changer l'adresse des pointeurs
Les adresse des pointeurs de tableaux ne peuvent pas être changé

### Exercice 5 ###

Voir photos

### Exercice 6 ###

```c
*ptr+2                     -> 14;
*(ptr+2)                   -> 34;
*&iptr+1                   -> Addresse de tabA[1]
&tabA[4] - 3               -> Adresse de tabA[1]
tabA+3                     -> &tabA[3];
&tab[7] - iptr             -> 7;
iptr+(*iptr-10)            -> &tab[2];
*(iptr+*(iptr+8)-tabA[7]); -> 23;
```

### Exercice 7 ###

Question 7.1

```c
int main(void) {
  int tab[5] = {6,7,1,2,4};
  int *p;
  p = tab;

  for(p = tab; p < &tab[5]; p++) {
    printf("%d",  *p);
  }

  return 0;
}
```

### Exercice ###

**Faire une matrice qui retourne une matrice dynamique de taille n lignes \* m colonnes**

```c
int ** creer_matrice(int n, int m){
  int ** A = malloc(sizeof(int *)*n);

  for(int i = 0; i < m; i++){
    A[i] = malloc(m * sizeof(int));
  }

  for(int i = 0; i < n; i++) {
    for(int j = 0; j < m; j++){
      A[i][j] = 0;
    }
  }

  return A;
}
```

**Libération de mémoire de cette matrice dynamique**

```c
void liberer(int **a, int n, int m){
  for(int i = 0; i < n; i++){
    free(A[i]);
  }
  free(A);
}
```
