# Manipulation de matrice dynamique #

1)  Ecrire un programme qui permet de construire une matrice NULL de taille Ligne \* Colonne.
2)  Modifier le programme pour construire la matrice carré identité I.
3)  Ecrire une fonction qui calcule la trace d'une matrice carrée.
4)  Ecrire une fonction qui si un element x appartient à une matrice A.
5)  Definir la fonction addition_mat qui peremet de faire l'addition de deux matrice de même dimension
6)  Concervoir une fonction qui verifie si une matrice carre est un carré magique
