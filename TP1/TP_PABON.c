/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TP_PABON.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/29 15:54:05 by NoobZik           #+#    #+#             */
/*   Updated: 2017/09/30 08:49:26 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * Note: Alors le main il sert juste à tester les différentes fonctions du prog
 * Il ne devrait pas avoir des dépassement de mémoire !
 * Le programme se compile sans warnings (Tout les warnings possible sont
 * activés) et erreurs.
 *
 * Le prof : Mario Valencia Pabon ne sait pas coder sans warning, son code de
 * créer matrice a été refactoré a ce qu'il n'y a pas de problème de warnings
 * (Conversion vers long unsigned int depuis int dans un malloc(sizeof)
 */

#include <stdlib.h>
#include <stdio.h>

int **creer_matrice(long unsigned int,long unsigned int);
void matrice_identite(int **, long unsigned int, long unsigned int);
void showMatrice(int **, long unsigned int, long unsigned int);
int **matriceCarre(int **, long unsigned int, long unsigned int);
void destructMatrice(int **, long unsigned int);
int **matriceAddition(int **, int **, long unsigned int,
                                       long unsigned int,
                                         long unsigned int,
                                         long unsigned int);

int main(int argc, char const *argv[]) {
  (void) argc;
  (void) argv;

  int **matA;
  int **matB;
  int **matC;

  matA = creer_matrice(3,3);
  matrice_identite(matA,3,3);
  showMatrice(matA,3,3);

  matB = matriceCarre(matA,3,3);
  printf("Matrice B\n");
  showMatrice(matB,3,3);

  matC = matriceAddition(matA, matB,3,3,3,3);
  printf("Matrice C:\n");
  showMatrice(matC,3,3);

  destructMatrice(matA,3);
  destructMatrice(matB,3);
  destructMatrice(matC,3);
  return 0;
}

/**
 * Peremet de creer une matrice dynamique.
 * Ceci est une version corrigé par moi meme, le prof code comme un pied.
 * Cette fonction est fonctionnel.
 * @param  n ligne
 * @param  m Colonne
 * @return   une matrice initialisé
 */
int **creer_matrice(long unsigned int n,long unsigned int m){
  int ** A = malloc(sizeof(int *)*n);
  long unsigned int i = 0,
                    j = 0;

  for(i = 0; i < m; i++){
    A[i] = malloc(m * sizeof(int));
  }

  for(i = 0; i < n; i++) {
    for(j = 0; j < m; j++){
      A[i][j] = 0;
    }
  }

  return A;
}

/**
 * Detruit proprement une matrice dynamiquement alloué
 * Cette fonction est fonctionnel
 * @param m Matrice
 * @param l Ligne
 * @param c Colonne
 */
void destructMatrice(int **m, long unsigned int l){
  long unsigned int i = 0;
          //          j = 0;

  while(i < l){
    free(m[i]);
    i++;
  }
  free(m);
}
/**
 * Remplis la matrice par une matrice identité
 * Une matrice identité est une matrice composé d'une diagonal 1
 * Fonction fonctionnel
 * @param m Matrice
 * @param l Ligne
 * @param c Colonne
 */
void matrice_identite(int **m, long unsigned int l, long unsigned int c){
  unsigned long int i = 0,
                    j = 0;

  while(i < l){
    while(j < c){
      if(i == j)
        m[i][j] = 1;
      else
        m[i][j] = 0;
      j++;
      }
    i++;
    j = 0;
  }
  return;
}

/**
 * Affiche la matrice tout simplement
 * @param m Matrice
 * @param l Ligne
 * @param c Colonne
 */
void showMatrice(int **m, long unsigned int l, long unsigned int c){
  unsigned long int i = 0,
                    j = 0;

  while(i<l){
    while(j<c){
      printf("%d ", m[i][j]);
      j++;
    }
    printf("\n");
    i++;
    j = 0;
  }
}

/**
 * Calcule d'une matrice Carrée
 * Fonction fonctionnel
 * @param  m  Matrice
 * @param  l  Ligne
 * @param  c  Colonne
 * @return   Le resultat d'une matriceCarre
 */
int **matriceCarre(int **m, long unsigned int l, long unsigned int c){
  int **res;
  long unsigned int i = 0,
                    j = 0,
                    k = 0;

  res = creer_matrice(l,c);

  while(i < l){
    while(j < c){
      while(k < l){
        res[i][j] += m[i][k] * m[k][j];
        k++;
      }
      k = 0;
      j++;
    }
    j = 0;
    i++;
  }
  return res;
}

/**
 * Permet de faire la somme de deux matrices
 * Fonction fonctionnel
 * @param  m Matrice 1
 * @param  n Matrice 2
 * @param  i Ligne mat 1
 * @param  j Ligne mat 2
 * @param  k Colonne mat 1
 * @param  l Colonne mat 2
 * @return   Matrice resultat
 */
int **matriceAddition(int **m,int **n, long unsigned int i,
                                       long unsigned int j,
                                         long unsigned int k,
                                         long unsigned int l) {

  int **res;

  long unsigned int a = 0;
  long unsigned int b = 0;

  if(i != k && j != l)
    return NULL;
  else{
    res = creer_matrice(i,j);
    while(a < i){
      while(b < j){
        res[a][b] = m[a][b] + n[a][b];
        b++;
      }
      b = 0;
      a++;
    }
  }
  return res;
}
