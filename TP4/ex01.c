/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex01.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/17 15:43:22 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/18 14:11:30 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

typedef struct st_node {
	int             key;
	struct st_node *left;
	struct st_node *right;
}                 node_t;

node_t* build           (int v,     node_t* L,    node_t* R);
void    print_inorder   (node_t* A);
node_t *search          (node_t* A, int v);
int     minimum         (node_t* A);
int     maximum         (node_t* A);
int     number_of_nodes (node_t* A);
int     height          (node_t* A);
void    insert          (int v,     node_t** A);
int     delete          (int v,     node_t** A);
void    free_tree       (node_t* A);

/* Construit un arbre a partir de la valeur v de la clef de la racine
et des sous-arbres droit (rs) et gauche (ls) */

node_t* build (int v, node_t* L, node_t* R) {
	node_t* nw = malloc(sizeof(node_t));
	nw->key = v;
	nw->left = L;
	nw->right = R;

	return nw;
}

/* Impression infixe d'un arbre binaire */

void print_inorder (node_t* A) {
	if (A != NULL) {
		print_inorder(A->left);
		printf("%d ", A->key);
		print_inorder(A->right);
	}
}

/* Recherche de v dans l'arbre binaire de recherche A */

node_t* search (node_t* A, int v) {

	if (A == NULL)   return NULL;

	if (v == A->key) return A;

	if (v < A->key) return search(A->left, v);
	else  return search(A->right, v);

}
/* Recherche du minimum dans un arbre binaire de recherche */

int minimum (node_t* A) {
	if (A == NULL)       return -1;
	if (A->left == NULL) return A->key;
	return minimum(A->left);
}
/* Recherche du maximum */

int maximum (node_t* A) {
	if (A == NULL)         return -1;
	if (A->right == NULL)  return A->key;
	return maximum(A->right);
}

/* Comptage du nombre d'elements contenus dans l'arbre A */

int number_of_nodes (node_t* A) {
	if (A == NULL) return -1;
	return 1 + number_of_nodes(A->left)
	         + number_of_nodes(A->right);
}

/* Mesure de la hauteur de l'arbre A */

int height (node_t* A) {
	int h1 = height(A->left);
	int hr = height(A->right);

	if (A == NULL) return -1;
	if (h1 > hr)   return 1 + h1;
	return 1 + hr;
}

/* Insertion de v dans l'arbre A */

void insert (int v, node_t** A) {
	if ( *A == NULL) {
		*A = build(v, NULL, NULL);
		return;
	}
	(v < (*A)->key) ?
		insert(v, &(*A)->left):
		insert(v, &(*A)->right);
  return;
}

/* Recherche puis suppression de v dans l'arbre A */

int delete (int v, node_t** A) {
	if ((*A) == NULL) return 0;
	if (v == (*A)->key) {
		if ((*A)->left == NULL) {
			node_t *tmp = (*A)->right;
			free(*A);
			(*A) = tmp;
			return 1;
		}
		if ((*A)->right == NULL) {
			node_t *tmp = (*A)->left;
			free(*A);
			(*A) = tmp;
			return 1;
		}
		int successor = minimum((*A)->right);
		delete(successor, &(*A)->right);
		(*A)->key = successor;
		return 1;
	}
	else {
		if (v < (*A)->key)
			return delete(v, &((*A)->left));
		else
			return delete(v, &((*A)->right));
	}
}
/* Liberation complete de la memoire occupee par un arbre */

void free_tree ( node_t* A) {
	if (A != NULL) {
		free_tree(A->left);
		free_tree(A->right);
		free(A);
	}
}
