/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exercice_1.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/06 15:49:55 by NoobZik           #+#    #+#             */
/*   Updated: 2018/09/23 20:30:01 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "Headers/pile.h"

int evaluatePoland(char const **, t_pile_list *, int);
int applyOperation(int, int, char);
bool checkOperator(char);

int main(int argc, char const *argv[]) {
	if (argc > 0) {
		int res;
		printf("hguft %s\n", argv[1]);
		t_pile_list *p = pile_create();

		res = evaluatePoland(argv,p, argc);

		printf("Le resultat est : %d\n", res);

		pile_free(p);
	}
	return EXIT_SUCCESS;
}


/**
 * evaluatePoland Evaluate a calculus from poland format
 * @param buffer String formatted entry
 * @param p      pile list
 * @return the Result of the operation
 */
int evaluatePoland(char const **buffer, t_pile_list *p, int lenght){
	int i = -1;
	t_pile_data tmp, tmp_a, tmp_b;

	puts("Debug :");

	while(++i < lenght) {
		puts(buffer[i]);
		while(!checkOperator(*buffer[i])) {
			tmp.number = atoi(buffer[i]);
			pile_stack(p, tmp);
			continue;
		}
		if (checkOperator(*buffer[i])) {
			tmp_a = pile_unstack(p);
			tmp_b = pile_unstack(p);
			tmp.number = applyOperation(tmp_a.number, tmp_b.number, *buffer[i]);
			pile_stack(p, tmp);
		}
	}
	tmp_b = pile_unstack(p);
	return tmp_b.number;
}

/**
 * Regarde dans la chaine de caractère si c'est un opérateur ou pas
 * @param c Un charactère du buffer de type char
 * @return Un booleen
 */
bool checkOperator(char c){
	if (c == '+' ||
	    c == '-' ||
      c == '*' ||
      c == '/')
      return true;
	return false;
}

/**
 * Apply operator according to the operator
 * @param a number A
 * @param b number B
 * @param o Operator
 * @return Result of the operation
 */
int applyOperation(int a, int b, char o){
	if(o == 'x')
		return a * b;
	if(o == '+')
		return a + b;
	if(o == '-')
		return a - b;
	if(o == '/')
		return a / b;
	return 0;
}
