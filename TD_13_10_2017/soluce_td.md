# TD2 du 13/10/2017 Structure de données Algorithme #

### **Exercice ?** ###


```c
void copier(F1,F2,F3){
	int e;
	while(!estVide(F1)){
		e = defiler(F1);		// O(1)
		enfiler(F2,e);			// O(1)
		enfiler(F3,e);			// O(1)
	}
	while(!estVide(F3))
		enfiler(F1, defiler(F3));
}

/** Compléxité
  n * 3 * O(1)
	+ n * 2 O(1
= O(N)
*/
 ```

```c
void copier(F1,F2){
	enfiler(F1, *);
	while(e=defiler(F1) != *){
		enfiler(F2,e);
		enfiler(F1,e);
	}
}
```
### **Exercice 3.6** ###

```c
void rot(File F){
	enfiler(F,defiler(F));
}
```

### **Exercice 3.7** ###

```c
bool contientPair(File F){
	bool res = false;
	enfiler(F,*);
	while(e = defiler(F) != *){
		if(e%z == 0)
			res = true;
		enfiler(F,e);
	}
	return res;
}
```

#### Q.3.7bis ####

*Sans utiliser \*, sans structures supp, récursion autorisé*

```c
int taille(File F){
	int n = 0;
	while(!estVide(F))
		n++,defiler(F);
	return n;
}
```

```c
bool ContientPair(File F){
	while(!estVide(F)){
		e=defiler(F);
		(e%2==0)?
		res=vrai:
		res=faux;
	}
}
```

**Ecrire une fonction en utilisant une pile qui calcule la taille d'une file**

```c
int taille(File F){
	int n = 0;
	while(!estVide(F)){
		empiler(p,defiler(F));
		n++;
	}
	while(!estVide(P))
		emfiler(F,depiler(p));
	while(!estVide(F))
		empiler(P,defiler(F));
	while(!estVide((P)))
		enfiler(F, depiler(P));
}
```

```c
void renverser(File F){
	if(estVide(F)) return F;
	else {
		e = defiler(F);
		renverser(F);
		enfiler(F,e);
	}
}
```
