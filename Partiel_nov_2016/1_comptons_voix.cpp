/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   1_comptons_voix.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/09 18:09:47 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/09 19:35:05 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * Q1.1
 * Cette fonction doit calculer le nombdre de voix reçues par la personne x,
 * parmis les votes stockés entre les indices i et j du tableau votes.
 * @param x Personne
 * @param v Tableau votes
 * @param i Indice de tableau
 * @param y Un autre indice du tableu
 * @return size_t
 *
 * La compléxité de cette algo est O(j-i)
 */
int occurences (Personne x, Votes v, int i ,int j) {
	int res = 0;
	int iter = i - 1;
	int moyenne = (v->n)/2;

	while (++iter < j) {
		(egale(v->tab[iter],x)) ? res++ : continue;
	}

	return res;
}

/**
 * Q1.2
 * Majorité abosolue
 *
 * Complexité : n puissance 3
 *
 * @param v [description]
 * @return [description]
 */
bool maj_absolue (Votes v) {
	int counter_1 = 0;
	int counter_2= 0;

	Personne x = v->tab[0];
	Personne y = 0;

	int i = 0;
	while (y == 0 || ++i < v->n){
		if (!(egale(x, v->tab[i])) {
			y = v->tab[i];
			break;
		}
		continue;
	}
	counter_1 = occurences(x, v->n);
	counter_2 = occurences(y, v->n);

	(counter_2 > moyenne || counter_1 > moyenne) ?
		return true:
		return false;

}

/**
 * Q1.3
 * Il suffit de trier le tableau avec un algo de O(nlogn)
 * ensuite boucle itérative
 * On a une variable Occurence maximal et occurence actuel, si la personne actuel
 * est égale au précédent, +1 a occurence actuel, sinon on remet à 0.
 * (Si occurence actuel est plus grand que max, max prends actuel ensuite remise
 * à zéro de actuel).
 *
 * Oui il est possible de le faire en O(n), il faut connaitre à l'avance
 * tout les personnes qui peuvent être voté. Creer un tableau de counter de taille
 * x personne et parcourir le tableau juste une fois en mettant à jours les counters.
 */

/**
 * Q1.4
 * Créer une pile représentant la liste des personnes du tableau
 * Trier le tableau en O(n long n)
 * Stack la premiere personne du tableau
 * Boucle itérative :
 * 	**Recherche de la personne suivante ne faisant pas partie parmis la liste.
 *
 * Complxité : O(n log(n) + n puissance jsais pas combien)
 * Parce que faut parcourir la liste a chaque test....
 * Espace... : mdr
 */
