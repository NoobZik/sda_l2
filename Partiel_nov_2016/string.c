inline void my_strcpy(char * restrict sd, const char * restrict sc)
{
	while ((*sd++ = *sc++) != '\0');
}
