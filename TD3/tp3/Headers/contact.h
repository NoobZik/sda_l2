/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   contact.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/13 17:34:01 by NoobZik           #+#    #+#             */
/*   Updated: 2017/10/13 17:48:48 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _CONTACT_H_
#define _CONTACT_H_

/* Helpers */

#include "Structures/contact.h"

/* Usage fonction */

contact_t *createContact	(char *,int, char *, char *);
void 			editName				(contact_t *, char const*);
void 			showContact			(contact_t);
void 			freeContact			(contact_t *);

#endif // _CONTACT_H_
