/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   contact.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/13 17:29:22 by NoobZik           #+#    #+#             */
/*   Updated: 2017/10/13 17:50:04 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTACT_STRUCTURE_H_
#define CONTACT_STRUCTURE_H_

typedef struct contact_s{
	char				*name;
	int					number;
	char				*mail;
	char				*pro;
}							contact_t;


#endif // CONTACT_STRUCTURE_H
