/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   listes.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/13 18:01:27 by NoobZik           #+#    #+#             */
/*   Updated: 2017/10/13 18:28:59 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _LISTES_STRUCTURES_H
#define _LISTES_STRUCTURES_H

#include "contact.h"

typedef struct	list_data_s{
	contact_t *		data;
	struct list_data_s *next;
	struct list_data_s *prec;
}								list_data_t;

typedef struct	listes_s {
	list_data_t		*first;
	list_data_t		*last;
	int						size;
} 							listes_t;

#endif // _LISTES_STRUCTURES_H
