/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   listes.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/13 18:06:09 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/09 18:00:03 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _LISTES_H_
#define _LISTES_H_

/* Helpers */

#include "Structures/listes.h"

/* Usage functions */

listes_t	*		listesCreate		(void);
void 					listesStack			(listes_t *, contact_t*);
list_data_t		listesUnstack		(listes_t *);
void 					listesDestruct	(list_data_t);
list_data_t * listesUnStack		(listes_t *l);
#endif // _LISTES_H_
