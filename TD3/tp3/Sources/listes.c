/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   listes.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/13 18:15:59 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/09 18:00:11 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

/* Helpers */

#include "../Headers/listes.h"

listes_t	*		listesCreate		(void){
	listes_t * l;
	l = 0;
	if (!(l = malloc(sizeof(listes_t)))) return l;
	l->first = 0;
	l->last = 0;
	l->size = 0;
	return l;
}

void		listesStack (listes_t *l, contact_t* p){
		list_data_t *d = malloc(sizeof(list_data_t));
		d->data = p;

		if (l->size){
			d->next = l->first;
			l->first = d;
		}
		else {
			l->first = d;
			l->last = d;
		}
		l++;
		return;
}

list_data_t * listesUnStack (listes_t *l) {
		list_data_t *unstacked = l->first;
		l->first = l->first->next;
		return unstacked;
}
