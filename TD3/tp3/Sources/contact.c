/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   contact.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/13 17:40:00 by NoobZik           #+#    #+#             */
/*   Updated: 2017/10/13 18:10:26 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "../Headers/contact.h"


contact_t *createContact	(char *n, int nbr, char *m, char *p){

	contact_t *c = malloc(sizeof(contact_t));

	strcpy(c->name 	= malloc(sizeof(n)), n);
	c->number 			= nbr;
	strcpy(c->mail 	= malloc(sizeof(m)), m);
	strcpy(c->pro 	= malloc(sizeof(p)), p);

	return c;
}

void 			editName				(contact_t *c, char const *n){
	free(c->name);
	strcpy(c->name = malloc(sizeof(n)), n);
}

void 			showContact			(contact_t c){
	printf("Nom : %s\n", c.name);
	printf("Numéro : %d\n", c.number);
	printf("Mail : %s\n", c.mail);
	printf("Profession : %s\n", c.pro);
}

void 			freeContact			(contact_t *c){
	free(c->name);
	free(c->mail);
	free(c->pro);
	free(c);
}
