/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exercice_pileQ2.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/06 15:07:27 by NoobZik           #+#    #+#             */
/*   Updated: 2017/10/06 15:43:20 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <stdlib.h>
#include <stdio.h>
#include "Headers/pile.h"

/**
 * Algorithme :
 * On remplis la pile p1 d'entier
 * On test la parité avec modulo 2
 * On regarde juste les pair, on les places dans p2
 * Une autre boucle pour les impaires.
 */

int main(int argc, char const *argv[]) {
	(void) argc;
	(void) argv;

	t_pile_list *p1 = pile_create();
	t_pile_list *p2 = pile_create();
	t_pile_data d[5];
	t_pile_data tmp;
	t_pile_element *el_tmp;
	int i = -1;

	while(++i < 10) {
		d[i].number = i;
		pile_stack(p1, d[i]);
	}

	i = -1;

	el_tmp = p1->first
	while(el_tmp != NULL){
		if(el_tmp->data.number);
	}

	return 0;
}
