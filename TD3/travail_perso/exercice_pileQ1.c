/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exercice_pile.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/06 14:03:46 by NoobZik           #+#    #+#             */
/*   Updated: 2017/10/06 15:01:00 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <stdlib.h>
#include <stdio.h>
#include "Headers/pile.h"

/**
 * Algorithme :
 * On remplis la pile p1 d'entier
 * On test si le sommet de la pile est égale à modulo 0
 * SI c'est vrai on la place dans P2 sinon dans P3
 */

int main(int argc, char const *argv[]) {
	(void) argc;
	(void) argv;

	t_pile_list *p1 = pile_create();
	t_pile_list *p2 = pile_create();
	t_pile_list *p3 = pile_create();
	t_pile_data d[5];
	t_pile_data tmp;
	t_pile_element *el_tmp;
	int i = -1;

	while(++i < 5) {
		d[i].number = i;
		pile_stack(p1, d[i]);
	}

	i = -1;

	while(p1->size){
		tmp = pile_unstack(p1);
		(tmp.number % 2 == 0)?
			pile_stack(p2,tmp):
			pile_stack(p3,tmp);
	}

	pile_free(p1);

	el_tmp = p2->first;
	printf("Pile 2 pair :");

	while(el_tmp != NULL){
		printf(" %d ", el_tmp->data.number);
		el_tmp = el_tmp->next;
	}

	pile_free(p2);

	printf("\nPile 3 impair :");
	el_tmp = p3->first;
	while(el_tmp != NULL){
		printf(" %d ", el_tmp->data.number);
		el_tmp = el_tmp->next;
	}

	printf("\n");
	pile_free(p3);

	return 0;
}
